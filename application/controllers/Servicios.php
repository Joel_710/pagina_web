<?php
    class Servicios extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Servicio');

      }
      //Funcion que renderiza la vista index                                        
      public function index(){
        $data['servicios']=$this->Servicio->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('servicios/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('servicios/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoServicio=array("nombres_ser"=>$this->input->post('nombres_ser'),
        "apellidos_ser"=>$this->input->post('apellidos_ser'),
        "tipo_ser"=>$this->input->post('tipo_ser'),        
        "telefono_ser"=>$this->input->post('telefono_ser'),
        "direccion_ser"=>$this->input->post('direccion_ser')
      );
        if($this->Servicio->insertar($datosNuevoServicio)){
          redirect('servicios/index');

        }else{
          echo "<h1>ERROR DE LA PAGINA</h1>";
        }

      }
      //funcion para eliminar instructores
      public function eliminar($id_ser){
        if($this->Servicio->
        borrar($id_ser)) {
        redirect('servicios/index');
        } else {
          echo "ERROR AL BORRAR :(";
        } 
      }
      
    } // Cierre de la clase
?>