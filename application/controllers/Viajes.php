<?php
    class Viajes extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Viajar');

      }
      //Funcion que renderiza la vista index                                        
      public function index(){
        $data['viajes']=$this->Viajar->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('viajes/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('viajes/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoViajar=array("empresa_via"=>$this->input->post('empresa_via'),
        "fecha_via"=>$this->input->post('fecha_via'),
        "ruc_via"=>$this->input->post('ruc_via'),
        "nombres_via"=>$this->input->post('nombres_via'),
        "apellidos_via"=>$this->input->post('apellidos_via'),
        "tipo_via"=>$this->input->post('tipo_via'),        
        "telefono_via"=>$this->input->post('telefono_via'),
        "direccion_via"=>$this->input->post('direccion_via')
      );
        if($this->Viajar->insertar($datosNuevoViajar)){
          redirect('viajes/index');

        }else{
          echo "<h1>ERROR DE LA PAGINA</h1>";
        }

      }
      //funcion para eliminar instructores
      public function eliminar($id_via){
        if($this->Viajar->
        borrar($id_via)) {
        redirect('viajes/index');
        } else {
          echo "ERROR AL BORRAR :(";
        } 
      }
      
    } // Cierre de la clase
?>