<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
        <img src="<?php echo base_url(); ?>/assets/images/s1.jpg" alt="" style="width: 200px; height: 200px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE SERVICIOS</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('servicios/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Servicio
    </a>
</div>
<br>
<br>
<br>
<?php if ($servicios) : ?>
    <table class="table table=striped table-bordered table-hover">
      <thead>
        <tr>
            <th>ID</th>
            <th>NOMBRES</th>
            <th>APELLIDOS</th>
            <th>TIPO SERVICIO</th>
            <th>TELEFONO</th>
            <th>DIRECCION</th>
            <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
            <?php foreach ($servicios
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_ser; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->nombres_ser; ?>
                  </td>
                  <td>
                    <?php echo 
                    $filaTemporal->apellidos_ser; ?>
                  </td>
                  <td>
                    <?php echo 
                    $filaTemporal->tipo_ser; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->telefono_ser; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->direccion_ser; ?>
                  </td>
                  <td class="text-center">
                    <a href="#" title="Editar Servicio">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url(); ?>/servicios/eliminar/<?php echo $filaTemporal->id_ser; ?>" 
                    title="Eliminar Servicio" 
                    onclick="return confirm('¿Estas seguro?');"
                    style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>
                  
              </tr>
            <?php endforeach; ?>
        </tbody>
        
    </table>
<?php else : ?>
    <h1> Dont have Servicios<h1>
        <?php endif; ?>