<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/01.png" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>




<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>NUEVO VIAJE</b></h1>
            <br>
        </div>
    </div>
</div>

<div class="container">
<form class="" action="<?php echo site_url();?>/viajes/guardar" method="post">
    <div class="row">
        <div class="col-md-4 text-center">
        <label for="">EMPRESA:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la empresa"
        class="form-control" name="empresa_via" value="">
        </div>
        <div class="col-md-2 text-center">
        <label for="">FECHA:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la Fecha"
        class="form-control" name="fecha_via" value="">
        </div>
        <div class="col-md-4 text-center">
        <label for="">RUC:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el Ruc"
        class="form-control" name="ruc_via" value="">
        </div>
        <div class="col-md-4 text-center">
        <label for="">NOMBRES:</label>
        <br>
        <input type="text"
        placeholder="Ingrese sus nombres"
        class="form-control" name="nombres_via" value="">
        </div>
        <div class="col-md-4 text-center">
        <label for="">APELLIDOS:</label>
        <br>
        <input type="text"
        placeholder="Ingrese sus apellidos"
        class="form-control" name="apellidos_via" value="">
        </div>
        <div class="col-md-2 text-center">
        <label for="">TIPO DE LICENCIA:</label>
        <br>
        <input type="text"
        placeholder="Ingrese su título"
        class="form-control" name="tipo_via" value="">
        </div>
        <div class="col-md-4 text-center">
        <label for="">TELÉFONO:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el teléfono"
        class="form-control" name="telefono_via" value="">
        </div>
        <div class="col-md-6 text-center">
        <label for="">DIRECCIÓN:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la dirección"
        class="form-control" name="direccion_via" value="">
        </div>
    </div>
    <br>
    <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
        GUARDAR
        </button>
        &nbsp;
        <a href="<?php echo site_url(); ?>/viajes/index" class="btn btn-danger">CANCELAR</a>
    </div>
    <br>
</form>
</div>
<br>
<br>
