<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1 style="color:red;"><b>COMTRANS MENA S.A</b></h1>
    </div>
  </div>
</div>




<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/images/t1.jpg" alt="imagen 1" style="width: 100%; height: 600px;">
    </div>

    <div class="item">
			<img src="<?php echo base_url(); ?>/assets/images/t2.jpg" alt="imagen 2" style="width: 100%; height: 600px;">
    </div>

    <div class="item">
			<img src="<?php echo base_url(); ?>/assets/images/t3.jpg" alt="imagen 3" style="width: 100%; height: 600px;">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<br>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h2 style="color:red;"><b>Misión</b></h2>
			<p>
				Nuestra misión es brindar un servicio de transporte de carga pesada a nivel nacional, cumpliendo con los estándares 
        de seguridad y tiempos de entrega pactados con nuestros clientes, y brindarles soluciones logísticas integrales 
        necesarias para sus operaciones que les permita enfocarse en sus negocios.
			</p>
		</div>
		<div class="col-md-6">
			<h2 style="color:red;"><b>Visión</b></h2>
			<p>
				Nuestra visión es dar soluciones eficientes a las necesidades logísticas y de transporte por carretera, 
        ofreciendo a nuestros clientes un servicio integral y personalizado, de calidad y respetuoso con el medio ambiente.
			</p>
		</div>
    <div class="col-md-12">
			<h2 style="color:red;"><b>Nosotros</b></h2>
			<p>
      La empresa Transportes Mena Cia. Ltda. se dedica a todas las actividades de transporte de carga por carretera: 
      troncos, ganado, transporte refrigerado, carga pesada, carga a granel, incluido el transporte en camiones cisterna,
      desperdicios y materiales de desecho, sin recogida ni eliminación.			</p>
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/images/01.png" alt="" style="width: 150px; height: 150px;">
                <div class="caption">
                    <h3 class="text-center"><b>VIAJES</b></h3>
                    <h5 class="text-center">Ingrese para mas información.</h5>
                    <p class="text-center"><a href="<?php echo site_url() ?>/viajes/nuevo" class="btn btn-primary" role="button">Visitar</a></p>
                </div>

            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/images/l2.jpg" alt="" style="width: 150px; height: 150px;">
                <div class="caption">
                    <h3 class="text-center"><b>SERVICIOS</b></h3>
                    <h5 class="text-center">Ingrese para mas información.</h5>
                    <p class="text-center"><a href="<?php echo site_url() ?>/servicios/nuevo" class="btn btn-primary" role="button">Visitar</a></p>
                </div>

            </div>
        </div>


        <div class="col-md-4">
            <div class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/images/l3.jpg" alt="" style="width: 150px; height: 150px;">
                <div class="caption">
                <h3 class="text-center"><b>CONTACTOS</b></h3>
                    <h5 class="text-center">Ingrese para mas información.</h5>
                    <p class="text-center"><a href="<?php echo site_url() ?>/contactos/nuevo" class="btn btn-primary" role="button">Visitar</a></p>
                </div>

            </div>
        </div>

    </div>
</div>