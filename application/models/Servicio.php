<?php

    class Servicio extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un servicio
        function insertar($datos){
            return $this->db->insert("servicio", $datos);

        }
        //Funcion para consultar servicios
        function obtenerTodos(){
            $listadoServicios=
            $this->db->get("servicio");
            
            if($listadoServicios
                ->num_rows()>0){//Si hay datos
                    return $listadoServicios->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar servicio
        function borrar($id_ser){
            $this->db->where("id_ser",$id_ser);
            return $this->db->delete("servicio"); 
        }

    }//Cierre de la clase
?>