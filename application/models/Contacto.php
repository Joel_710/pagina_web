<?php

    class Contacto extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un viaje
        function insertar($datos){
            return $this->db->insert("contacto", $datos);

        }
        //Funcion para consultar Viajes
        function obtenerTodos(){
            $listadoContactos=
            $this->db->get("contacto");
            
            if($listadoContactos
                ->num_rows()>0){//Si hay datos
                    return $listadoContactos->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar Viajes
        function borrar($id_con){
            $this->db->where("id_con",$id_con);
            return $this->db->delete("contacto"); 
        }

    }//Cierre de la clase
?>