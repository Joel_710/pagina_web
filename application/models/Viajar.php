<?php

    class Viajar extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un viaje
        function insertar($datos){
            return $this->db->insert("viajar", $datos);

        }
        //Funcion para consultar Viajes
        function obtenerTodos(){
            $listadoViajes=
            $this->db->get("viajar");
            
            if($listadoViajes
                ->num_rows()>0){//Si hay datos
                    return $listadoViajes->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar Viajes
        function borrar($id_via){
            $this->db->where("id_via",$id_via);
            return $this->db->delete("viajar"); 
        }

    }//Cierre de la clase
?>